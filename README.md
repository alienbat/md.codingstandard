#Project Setup

## Use Cocoapod

Please use Cocoapod for thirdparty code management. 
Use `.xcworkspace` file instead of the `xcodeproj` file. 

## Thirdparty libraries

Thirdparty libraries must be referenced as a cocoapod and listed in `Podfile`. Do NOT add the source code of the library into the project directly. 

Please use the following libraries if the function requires. 

`AFNetwork` for networking

`Parse` if relavent

`SWRevealViewController` for side hamburger style UI

`MBProgressHUD` for showing waiting screens.

Please feel free to use other thirdparty libraries that do not functionally overlap the above ones when nessasary. 

## Use of Storyboard

Storyboard files and XIB files are allowed, however please make sure that __at most only one root view, or one view controller can present in a single XIB file or Storyboard file__. The transistions between view controllers must be done in the code. 

## Build targets and target specific configurations

A project should have two build targets, one points to the production environment, another to the test environment. They should use different `Info.plist` files for different configurations. Such configuration (that differenciates production and testing) must be coded in the `Info.plist`. __DO NOT USE__ `#ifdef` __macros for configurations.__ 

## iOS Deployment Target

THe iOS deployment target is iOS 7.1.

